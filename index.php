<?php include 'views/core/head.php'; ?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#browseModels">Models</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#browseAgenda">Agenda</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formBooking">Booking</button>

<?php include 'modal-agenda.php'; ?>
<?php include 'modal-booking.php'; ?>
<?php include 'modal-model.php'; ?>


<section class="platform platform-single embed-responsive embed-responsive-16by9">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/booths/large/3-1.mp4" type="video/mp4" />
    </video>
    <div class="car  car-top-1" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-1.png" class="block" />
        <div class="agent agent-female agent-right"></div>
    </div>
    <div class="car  car-top-2" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-2.png" class="block" />
        <div class="agent agent-female agent-right"></div>
    </div>
    <div class="car  car-top-3" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-3.png" class="block" />
        <div class="agent agent-left"></div>
    </div>
    <div class="car  car-low-1" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-4.png" class="block" />
        <div class="agent agent-left"></div>
    </div>
    <div class="car  car-low-2" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-1.png" class="block" />
        <!-- <div class="agent agent-female agent-right"></div> -->
    </div>
    <div class="car  car-low-3" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-2.png" class="block" />
        <div class="agent agent-female agent-right"></div>
    </div>
    <div class="brand  brand-desk"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
    <div class="brand  brand-top-left"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
    <div class="brand  brand-top-right"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
</section>




<!-- <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1099048012&color=%2300aabb&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe> -->

<?php include 'views/templates/toolbar.php'; ?>
<?php include 'views/core/foot.php'; ?>
