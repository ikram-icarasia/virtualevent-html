<!--MODAL:MODELS-->
<?php
    $modal_id       = 'browseModels';
    $modal_class    = 'modal-models';
    $modal_title    = 'Honda Models';
    include 'views/templates/modal-start.php';
?>
    <ul class="modal-tabs  nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#">Hatchback</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Sedan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">MPV</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">SUV</a>
        </li>
    </ul>
    <?php
        $modal_carousel_name = 'carouselExampleIndicators';
        include 'views/templates/modal-carousel.php';
    ?>
    <div class="modal-story">
        <h4 class="mb-2  text-uppercase  text-primary">Honda Civic Type-R</h4>
        <h6 class="mb-3  font-weight-normal  text-uppercase  text-success">RM 88,000 &ndash; RM 98,000</h6>
        MG ZS Feel The Energy! Desain mengesankan dengan daya kuat, MG ZS dirancang untuk menyesuaikan kebutuhan perjalanan sehari-hari.
        Dilengkapi dengan sistem interaktif yang cerdas, kinerja mesin yang efisien dan interior luas, MG ZS siap untuk menguji kekuatan hingga batasnya.

        MG ZS Feel The Energy! Desain mengesankan dengan daya kuat, MG ZS dirancang untuk menyesuaikan kebutuhan perjalanan sehari-hari.
        Dilengkapi dengan sistem interaktif yang cerdas, kinerja mesin yang efisien dan interior luas, MG ZS siap untuk menguji kekuatan hingga batasnya.
    </div>
    <div class="modal-actions  d-sm-flex  align-items-center  justify-content-between">
        <a href="https://content.icarcdn.com/200402_mg_brochure_lippo_combined.pdf" target="_blank" class="btn btn-dark">Download Broucher</a>
        <div class="modal-actions-couple  d-flex justify-content-between">
            <a href="https://www.mobil123.com/form/test-drive-dve2020-mg" target="_blank" class="btn btn-primary mr-1">Test Drive</a>
            <a href="https://www.mobil123.com/form/form-booking-dve2020-mg" target="_blank" class="btn btn-success ml-1">Make Booking</a>
        </div>
    </div>
<?php include 'views/templates/modal-end.php'; ?>
<!--MODAL:MODELS-->
