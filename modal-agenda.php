<!--MODAL:AGENDA-->
<?php
    $modal_id       = 'browseAgenda';
    $modal_class    = 'modal-agenda';
    $modal_title    = 'Agenda';

    include 'views/templates/modal-start.php';
?>
    <h4 class="agenda-date">30 August 2020</h4>
    <ul class="agenda  list-unstyled">
        <li class="past">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">Past Agenda</div>
            </div>
        </li>
        <li class="running">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">
                    <div>Running Agenda</div>
                    <div class="agenda-snippet">
                        <div class="row">
                            <div class="col-4 pr-0">
                                <div class="agenda-snippet-preview  embed-responsive embed-responsive-16by9">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/U3olJWzIKvY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="agenda-snippet-content">
                                    <div class="agenda-snippet-title">Snippet Content</div>
                                    <div class="agenda-snippet-url  h6  small  text-muted">https://www.youtube.com/watch?v=U3olJWzIKvY</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="upcoming">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">Upcoming Agenda</div>
            </div>
        </li>
    </ul>
    <hr class="divider" />
    <h4 class="agenda-date">31 August 2020</h4>
    <ul class="agenda  list-unstyled">
        <li class="past">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">Past Agenda</div>
            </div>
        </li>
        <li class="running">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">Running Agenda</div>
            </div>
        </li>
        <li class="upcoming">
            <div class="row">
                <div class="col-4 time">Time</div>
                <div class="col-8 title">Upcoming Agenda</div>
            </div>
        </li>
    </ul>
    <hr class="divider" />
    <h4 class="agenda-date">1 September 2020</h4>
    <ul class="agenda  list-unstyled">
    <li class="past">
        <div class="row">
            <div class="col-4 time">Time</div>
            <div class="col-8 title">Past Agenda</div>
        </div>
    </li>
    <li class="running">
        <div class="row">
            <div class="col-4 time">Time</div>
            <div class="col-8 title">Running Agenda</div>
        </div>
    </li>
    <li class="upcoming">
        <div class="row">
            <div class="col-4 time">Time</div>
            <div class="col-8 title">Upcoming Agenda</div>
        </div>
    </li>
</ul>
<?php include 'views/templates/modal-end.php'; ?>
<!--MODAL:AGENDA-->
