<?php
    include 'views/core/head.php';
    $hall_size = 8;
?>

<section class="platform  platform-hall  platform-hall-single  hall-elegant  embed-responsive embed-responsive-16by9">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-elegant.mp4" type="video/mp4" />
    </video>

    <?php include 'views/templates/cinema-billboards.php'; ?>

    <?php
    for($booth = 1; $booth <= $hall_size; $booth++ ) {
        $booth_side = 'right';
        if ($booth % 2) { $booth_side = 'left'; }
    ?>
        <a href="#" class="booth-logo booth-logo-<?=$booth?> booth-logo-<?=$booth_side?>" style="background-image: url('assets/booths/booth-cap.png')">
            <img src="assets/cars/honda/honda-logo.png" class="d-block" alt="Booth Brand" />
        </a>
        <a href="#" class="booth booth-<?=$booth?> booth-<?=$booth_side?>" style="background-image: url('assets/booths/booth-<?=$booth_side?>.png')">
            <div class="booth-tv"><img src="https://media.giphy.com/media/xT9DPDoWMicL4nU3NC/source.gif" class="d-block" /></div>
            <div class="booth-car car">
                <img class="car-image" src="assets/cars/mazda/car-<?= rand(1,7); ?>.png" class="block" alt="Car" />
            </div>
        </a>
        <a href="#" class="booth-rep  booth-rep-<?=$booth?>  booth-rep-<?=$booth_side?>">
            <img class="person" src="assets/avatars/avatar-<?= rand(1,4); ?>.gif" class="block" alt="Rep" />
        </a>
    <?php } ?>

    <div class="hall-logo"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
</section>

<?php include 'views/templates/toolbar.php'; ?>
<?php include 'views/core/foot.php'; ?>
