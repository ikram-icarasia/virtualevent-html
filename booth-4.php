<?php include 'views/core/head.php'; ?>

<?php include 'modal-booking.php'; ?>
<?php include 'modal-model.php'; ?>


<section class="platform  platform-booth  booth-4  embed-responsive  embed-responsive-16by9">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/booths/basic/4.mp4" type="video/mp4" />
    </video>

    <?php $booth_tv = 1; include 'views/templates/booth-tv.php'; ?>
    <?php $booth_tv = 2; include 'views/templates/booth-tv.php'; ?>

    <?php for($car = 1; $car <= 4; $car++ ) { ?>
    <div class="car  car-<?=$car?>" data-toggle="modal" data-target="#browseModels">
        <img class="car-image" src="assets/cars/honda/cars/honda-car-<?=rand(1,4)?>.png" class="block" />
    </div>
    <?php } ?>
    
    <div class="brand  brand-1"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
    <div class="brand  brand-2"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
    <div class="brand  brand-3"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
</section>




<!-- <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1099048012&color=%2300aabb&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe> -->

<?php include 'views/templates/toolbar.php'; ?>
<?php include 'views/core/foot.php'; ?>
