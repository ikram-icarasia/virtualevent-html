<a href="#" class="cinema">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-left-top">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-left-middle">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-left-bottom">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-right-top">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-right-middle">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
<a href="#" class="billboard  billboard-right-bottom">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-6.mp4" type="video/mp4" />
    </video>
</a>
