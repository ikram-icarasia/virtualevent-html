<div class="modal <?= $modal_class ?>" id="<?= $modal_id ?>" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex  align-items-center">
                <h5 class="modal-title"><?= $modal_title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
