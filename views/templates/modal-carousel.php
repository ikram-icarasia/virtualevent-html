<div id="<?= $modal_carousel_name ?>" class="modal-carousel carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/ia2z3i4h6hvigks6tr9o/drop-type-lx-shoe-0cSskV.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5,q_80/o80fx0zlhsvdsnvjlxur/drop-type-lx-shoe-0cSskV.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5,q_80/vipfftekp3b9tk4wnkma/drop-type-lx-shoe-0cSskV.jpg" class="d-block w-100" alt="...">
        </div>
    </div>
    <div class="carousel-navigation">
        <ol class="carousel-indicators">
            <li data-target="#<?= $modal_carousel_name ?>" data-slide-to="0" class="active"></li>
            <li data-target="#<?= $modal_carousel_name ?>" data-slide-to="1"></li>
            <li data-target="#<?= $modal_carousel_name ?>" data-slide-to="2"></li>
        </ol>
        <a class="carousel-control-prev" href="#<?= $modal_carousel_name ?>" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#<?= $modal_carousel_name ?>" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
