<nav class="toolbar  navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom">
    <div class="container">
        <a class="navbar-brand text-uppercase" href="#">Back to Hall</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto text-uppercase">
                <li class="nav-item">
                    <a class="nav-link" href="#">Chat with Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Our Models</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Test Drive</a>
                </li>
                <li class="nav-item">
                    <a class="btn  btn-primary" href="#">Make Booking</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
