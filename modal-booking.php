<!--MODAL:BOOKING-->
<?php
$modal_id       = 'formBooking';
$modal_class    = 'modal-booking';
$modal_title    = 'Make Booking - Model Name';

include 'views/templates/modal-start.php';
?>
<form class="jotform-form" action="https://submit.jotform.com/submit/202011737401439/" method="post" name="form_202011737401439" id="202011737401439" accept-charset="utf-8" autocomplete="on" novalidate="true">
    <input type="hidden" name="formID" value="202011737401439">
    <input type="hidden" id="JWTContainer" value="">
    <input type="hidden" id="cardinalOrderNumber" value="">
    <div role="main" class="form-all">
        <ul class="form-section page-section">
            <li class="form-line" data-type="control_image" id="id_18">
                <div id="cid_18" class="form-input-wide">
                    <div style="text-align:center">
                        <img alt="" class="form-image" style="border:0" src="https://www.jotform.com/uploads/icarasia/form_files/cover%20fb%20(1).5f1a42217a8c23.20728168.jpg" height="229px" width="618px" data-component="image">
                    </div>
                </div>
            </li>
            <li class="form-line jf-required form-line-error" data-type="control_textbox" id="id_4">
                <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4">
                    Nama
                    <span class="form-required">
                        *
                    </span>
                </label>
                <div id="cid_4" class="form-input-wide jf-required">
                    <input type="text" id="input_4" name="q4_nama" data-type="input-textbox" class="form-textbox validate[required, Alphabetic] form-validation-error" size="20" value="" placeholder="Masukan Nama Anda" data-component="textbox" aria-labelledby="label_4" required="">
                    <div class="form-error-message"><img src="https://cdn.jotfor.ms/images/exclamation-octagon.png"> This field is required.<div class="form-error-arrow"><div class="form-error-arrow-inner"></div></div></div></div>
                </li>
                <li class="form-line jf-required" data-type="control_textbox" id="id_15">
                    <label class="form-label form-label-top form-label-auto" id="label_15" for="input_15">
                        Nomor Telepon
                        <span class="form-required">
                            *
                        </span>
                    </label>
                    <div id="cid_15" class="form-input-wide jf-required">
                        <input type="text" id="input_15" name="q15_nomorTelepon" data-type="input-textbox" class="form-textbox validate[required, Numeric]" size="20" value="" maxlength="13" placeholder="Masukan Nomor Telepon" data-component="textbox" aria-labelledby="label_15" required="">
                    </div>
                </li>
                <li class="form-line jf-required" data-type="control_widget" id="id_17">
                    <label class="form-label form-label-top form-label-auto" id="label_17" for="input_17">
                        Kota
                        <span class="form-required">
                            *
                        </span>
                    </label>
                    <div id="cid_17" class="form-input-wide jf-required">
                        <div data-widget-name="Dynamic Dropdowns" style="width:100%;text-align:Left;overflow-x:auto" data-component="widget-field">
                            <iframe title="Dynamic Dropdowns" frameborder="0" scrolling="no" allowtransparency="true" allow="geolocation; microphone; camera; autoplay; encrypted-media; fullscreen" data-type="iframe" class="custom-field-frame custom-field-frame-rendered frame-xd-ready frame-ready" id="customFieldFrame_17" src="//widgets.jotform.io/dynamicDropdowns/?qid=17&amp;ref=https%3A%2F%2Fform.jotform.me&amp;injectCSS=false" style="max-width: 400px; border: none; width: 400px; height: 110px;" data-width="400" data-height="50">
                            </iframe>
                            <div class="widget-inputs-wrapper">
                                <input type="hidden" id="input_17" class="form-hidden form-widget widget-required " name="q17_kota" value="DKI Jakarta
                                Jakarta Selatan">
                                <input type="hidden" id="widget_settings_17" class="form-hidden form-widget-settings" value="%5B%7B%22name%22%3A%22list%22%2C%22value%22%3A%22DKI%20Jakarta%5Cn%20Jakarta%20Selatan%5Cn%20Jakarta%20Barat%5Cn%20Jakarta%20Utara%5Cn%20Jakarta%20Timur%5CnJawa%20Barat%5Cn%20Bandung%20%5Cn%20Banjar%20%5Cn%20Bekasi%20%5Cn%20Bogor%20%5Cn%20Cimahi%20%5Cn%20Cirebon%20%5Cn%20Depok%20%5Cn%20Sukabumi%20%5Cn%20Tasikmalaya%20%5Cn%20Bandung%20Kabupaten%5Cn%20Bekasi%20%5Cn%20Bogor%20%5Cn%20Ciamis%20%5Cn%20Cianjur%20%5Cn%20Cirebon%20%5Cn%20Garut%20%5Cn%20Indramayu%20%5Cn%20Karawang%20%5Cn%20Kuningan%20%5Cn%20Majalengka%20%5Cn%20Pangandaran%20%5Cn%20Purwakarta%20%5Cn%20Subang%20%5Cn%20Sukabumi%20%5Cn%20Sumedang%20%5Cn%20Tasikmalaya%20%5Cn%20Bandung%20Barat%5CnJawa%20Timur%5Cn%20Kabupaten%20Bangkalan%5Cn%20Kabupaten%20Banyuwangi%5Cn%20Kabupaten%20Blitar%5Cn%20Kabupaten%20Bojonegoro%5Cn%20Kabupaten%20Bondowoso%5Cn%20Kabupaten%20Gresik%5Cn%20Kabupaten%20Jember%5Cn%20Kabupaten%20Jombang%5Cn%20Kabupaten%20Kediri%5Cn%20Kabupaten%20Lamongan%5Cn%20Kabupaten%20Lumajang%5Cn%20Kabupaten%20Madiun%5Cn%20Kabupaten%20Magetan%5Cn%20Kabupaten%20Malang%5Cn%20Kabupaten%20Mojokerto%5Cn%20Kabupaten%20Nganjuk%5Cn%20Kabupaten%20Ngawi%5Cn%20Kabupaten%20Pacitan%5Cn%20Kabupaten%20Pamekasan%5Cn%20Kabupaten%20Pasuruan%5Cn%20Kabupaten%20Ponorogo%5Cn%20Kabupaten%20Probolinggo%5Cn%20Kabupaten%20Sampang%5Cn%20Kabupaten%20Sidoarjo%5Cn%20Kabupaten%20Situbondo%5Cn%20Kabupaten%20Sumenep%5Cn%20Kabupaten%20Trenggalek%5Cn%20Kabupaten%20Tuban%5Cn%20Kota%20Batu%5Cn%20Kota%20Blitar%5Cn%20Kota%20Kediri%5Cn%20Kota%20Madiun%5Cn%20Kota%20Malang%5Cn%20Kota%20Mojokerto%5Cn%20Kota%20Pasuruan%5Cn%20Kota%20Probolinggo%5Cn%20Kota%20Surabaya%5CnNanggroe%20Aceh%20Darussalam%5Cn%20Kabupaten%20Aceh%20Barat%5Cn%20Kabupaten%20Aceh%20Barat%20Daya%5Cn%20Kabupaten%20Aceh%20Besar%5Cn%20Kabupaten%20Aceh%20Jaya%5Cn%20Kabupaten%20Aceh%20Selatan%5Cn%20Kabupaten%20Aceh%20Singkil%5Cn%20Kabupaten%20Aceh%20Tamiang%5Cn%20Kabupaten%20Aceh%20Tengah%5Cn%20Kabupaten%20Aceh%20Tenggara%5Cn%20Kabupaten%20Aceh%20Timur%5Cn%20Kabupaten%20Aceh%20Utara%5Cn%20Kabupaten%20Bener%20Meriah%5Cn%20Kabupaten%20Bireuen%5Cn%20Kabupaten%20Gayo%20Lues%5Cn%20Kabupaten%20Nagan%20Raya%5Cn%20Kabupaten%20Pidie%5Cn%20Kabupaten%20Pidie%20Jaya%5Cn%20Kabupaten%20Simeulue%5Cn%20Kota%20Banda%20Aceh%5Cn%20Kota%20Langsa%5Cn%20Kota%20Lhokseumawe%5Cn%20Kota%20Sabang%5Cn%20Kota%20Subulussalam%5CnSumatera%20Utara%5Cn%20Kabupaten%20Asahan%5Cn%20Kabupaten%20Batu%20Bara%5Cn%20Kabupaten%20Dairi%5Cn%20Kabupaten%20Deli%20Serdang%5Cn%20Kabupaten%20Humbang%20Hasundutan%5Cn%20Kabupaten%20Karo%5Cn%20Kabupaten%20Labuhanbatu%5Cn%20Kabupaten%20Labuhanbatu%20Selatan%5Cn%20Kabupaten%20Labuhanbatu%20Utara%5Cn%20Kabupaten%20Langkat%5Cn%20Kabupaten%20Mandailing%20Natal%5Cn%20Kabupaten%20Nias%5Cn%20Kabupaten%20Nias%20Barat%5Cn%20Kabupaten%20Nias%20Selatan%5Cn%20Kabupaten%20Nias%20Utara%5Cn%20Kabupaten%20Padang%20Lawas%5Cn%20Kabupaten%20Padang%20Lawas%20Utara%5Cn%20Kabupaten%20Pakpak%20Bharat%5Cn%20Kabupaten%20Samosir%5Cn%20Kabupaten%20Serdang%20Bedagai%5Cn%20Kabupaten%20Simalungun%5Cn%20Kabupaten%20Tapanuli%20Selatan%5Cn%20Kabupaten%20Tapanuli%20Tengah%5Cn%20Kabupaten%20Tapanuli%20Utara%5Cn%20Kabupaten%20Toba%20Samosir%5Cn%20Kota%20Binjai%5Cn%20Kota%20Gunungsitoli%5Cn%20Kota%20Medan%5Cn%20Kota%20Padangsidempuan%5Cn%20Kota%20Pematangsiantar%5Cn%20Kota%20Sibolga%5Cn%20Kota%20Tanjungbalai%5Cn%20Kota%20Tebing%20Tinggi%5CnSumatera%20Barat%5Cn%20Kabupaten%20Agam%5Cn%20Kabupaten%20Dharmasraya%5Cn%20Kabupaten%20Kepulauan%20Mentawai%5Cn%20Kabupaten%20Lima%20Puluh%20Kota%5Cn%20Kabupaten%20Padang%20Pariaman%5Cn%20Kabupaten%20Pasaman%5Cn%20Kabupaten%20Pasaman%20Barat%5Cn%20Kabupaten%20Pesisir%20Selatan%5Cn%20Kabupaten%20Sijunjung%5Cn%20Kabupaten%20Solok%5Cn%20Kabupaten%20Solok%20Selatan%5Cn%20Kabupaten%20Tanah%20Datar%5Cn%20Kota%20Bukittinggi%5Cn%20Kota%20Padang%5Cn%20Kota%20Padangpanjang%5Cn%20Kota%20Pariaman%5Cn%20Kota%20Payakumbuh%5Cn%20Kota%20Sawahlunto%5Cn%20Kota%20Solok%5CnRiau%5Cn%20Kabupaten%20Bengkalis%5Cn%20Kabupaten%20Indragiri%20Hilir%5Cn%20Kabupaten%20Indragiri%20Hulu%5Cn%20Kabupaten%20Kampar%5Cn%20Kabupaten%20Kepulauan%20Meranti%5Cn%20Kabupaten%20Kuantan%20Singingi%5Cn%20Kabupaten%20Pelalawan%5Cn%20Kabupaten%20Rokan%20Hilir%5Cn%20Kabupaten%20Rokan%20Hulu%5Cn%20Kabupaten%20Siak%5Cn%20Kota%20Dumai%5Cn%20Kota%20Pekanbaru%5CnKepulauan%20Riau%5Cn%20Kabupaten%20Bintan%5Cn%20Kabupaten%20Karimun%5Cn%20Kabupaten%20Kepulauan%20Anambas%5Cn%20Kabupaten%20Lingga%5Cn%20Kabupaten%20Natuna%5Cn%20Kota%20Batam%5Cn%20Kota%20Tanjungpinang%5CnJambi%5Cn%20Kabupaten%20Batanghari%5Cn%20Kabupaten%20Bungo%5Cn%20Kabupaten%20Kerinci%5Cn%20Kabupaten%20Merangin%5Cn%20Kabupaten%20Muaro%20Jambi%5Cn%20Kabupaten%20Sarolangun%5Cn%20Kabupaten%20Tanjung%20Jabung%20Barat%5CnBengkulu%5Cn%20Kabupaten%20Bengkulu%20Selatan%5Cn%20Kabupaten%20Bengkulu%20Tengah%5Cn%20Kabupaten%20Bengkulu%20Utara%5Cn%20Kabupaten%20Kaur%5Cn%20Kabupaten%20Kepahiang%5Cn%20Kabupaten%20Lebong%5Cn%20Kabupaten%20Mukomuko%5Cn%20Kabupaten%20Rejang%20Lebong%5Cn%20Kabupaten%20Seluma%5Cn%20Kota%20Bengkulu%5CnSumatera%20Selatan%5Cn%20Kabupaten%20Banyuasin%5Cn%20Kabupaten%20Empat%20Lawang%5Cn%20Kabupaten%20Lahat%5Cn%20Kabupaten%20Muara%20Enim%5Cn%20Kabupaten%20Musi%20Banyuasin%5Cn%20Kabupaten%20Musi%20Rawas%5Cn%20Kabupaten%20Musi%20Rawas%20Utara%5Cn%20Kabupaten%20Ogan%20Ilir%5Cn%20Kabupaten%20Ogan%20Komering%20Ilir%5Cn%20Kabupaten%20Ogan%20Komering%20Ulu%5Cn%20Kabupaten%20Ogan%20Komering%20Ulu%20Selatan%5Cn%20Kabupaten%20Ogan%20Komering%20Ulu%20Timur%5Cn%20Kabupaten%20Penukal%20Abab%20Lematang%20Ilir%5Cn%20Kota%20Lubuklinggau%5Cn%20Kota%20Pagar%20Alam%5Cn%20Kota%20Palembang%5Cn%20Kota%20Prabumulih%5CnKepulauan%20Bangka%20Belitung%5Cn%20Kabupaten%20Bangka%5Cn%20Kabupaten%20Bangka%20Barat%5Cn%20Kabupaten%20Bangka%20Selatan%5Cn%20Kabupaten%20Bangka%20Tengah%5Cn%20Kabupaten%20Belitung%5Cn%20Kabupaten%20Belitung%20Timur%5Cn%20Kota%20Pangkalpinang%5CnLampung%5Cn%20Kabupaten%20Lampung%20Barat%5Cn%20Kabupaten%20Lampung%20Selatan%5Cn%20Kabupaten%20Lampung%20Tengah%5Cn%20Kabupaten%20Lampung%20Timur%5Cn%20Kabupaten%20Lampung%20Utara%5Cn%20Kabupaten%20Mesuji%5Cn%20Kabupaten%20Pesawaran%5Cn%20Kabupaten%20Pesisir%20Barat%5Cn%20Kabupaten%20Pringsewu%5Cn%20Kabupaten%20Tanggamus%5Cn%20Kabupaten%20Tulang%20Bawang%5Cn%20Kabupaten%20Tulang%20Bawang%20Barat%5Cn%20Kabupaten%20Way%20Kanan%5Cn%20Kota%20Bandar%20Lampung%5Cn%20Kota%20Metro%5CnBanten%5Cn%20Kabupaten%20Lebak%5Cn%20Kabupaten%20Pandeglang%5Cn%20Kabupaten%20Serang%5Cn%20Kabupaten%20Tangerang%5Cn%20Kota%20Cilegon%5Cn%20Kota%20Serang%5Cn%20Kota%20Tangerang%5Cn%20Kota%20Tangerang%20Selatan%5CnJawa%20Tengah%5Cn%20Kabupaten%20Banjarnegara%5Cn%20Kabupaten%20Banyumas%5Cn%20Kabupaten%20Batang%5Cn%20Kabupaten%20Blora%5Cn%20Kabupaten%20Boyolali%5Cn%20Kabupaten%20Brebes%5Cn%20Kabupaten%20Cilacap%5Cn%20Kabupaten%20Demak%5Cn%20Kabupaten%20Grobogan%5Cn%20Kabupaten%20Jepara%5Cn%20Kabupaten%20Karanganyar%5Cn%20Kabupaten%20Kebumen%5Cn%20Kabupaten%20Kendal%5Cn%20Kabupaten%20Klaten%5Cn%20Kabupaten%20Kudus%5Cn%20Kabupaten%20Magelang%5Cn%20Kabupaten%20Pati%5Cn%20Kabupaten%20Pekalongan%5Cn%20Kabupaten%20Pemalang%5Cn%20Kabupaten%20Purbalingga%5Cn%20Kabupaten%20Purworejo%5Cn%20Kabupaten%20Rembang%5Cn%20Kabupaten%20Semarang%5Cn%20Kabupaten%20Sragen%5Cn%20Kabupaten%20Sukoharjo%5Cn%20Kabupaten%20Tegal%5Cn%20Kabupaten%20Temanggung%5Cn%20Kabupaten%20Wonogiri%5Cn%20Kabupaten%20Wonosobo%5Cn%20Kota%20Magelang%5Cn%20Kota%20Pekalongan%5Cn%20Kota%20Salatiga%5Cn%20Kota%20Semarang%5Cn%20Kota%20Surakarta%5Cn%20Kota%20Tegal%5CnDI%20Yogyakarta%5Cn%20Kabupaten%20Bantul%5Cn%20Kabupaten%20Gunungkidul%5Cn%20Kabupaten%20Kulon%20Progo%5Cn%20Kabupaten%20Sleman%5Cn%20Kota%20Yogyakarta%5CnBali%5Cn%20Kabupaten%20Badung%5Cn%20Kabupaten%20Bangli%5Cn%20Kabupaten%20Buleleng%5Cn%20Kabupaten%20Gianyar%5Cn%20Kabupaten%20Jembrana%5Cn%20Kabupaten%20Karangasem%5Cn%20Kabupaten%20Klungkung%5Cn%20Kabupaten%20Tabanan%5Cn%20Kota%20Denpasar%5CnNusa%20Tenggara%20Barat%5Cn%20Kabupaten%20Bima%5Cn%20Kabupaten%20Dompu%5Cn%20Kabupaten%20Lombok%20Barat%5Cn%20Kabupaten%20Lombok%20Tengah%5Cn%20Kabupaten%20Lombok%20Timur%5Cn%20Kabupaten%20Lombok%20Utara%5Cn%20Kabupaten%20Sumbawa%5Cn%20Kabupaten%20Sumbawa%20Barat%5Cn%20Kota%20Bima%5Cn%20Kota%20Mataram%5CnNusa%20Tenggara%20Timur%5Cn%20Kabupaten%20Alor%5Cn%20Kabupaten%20Belu%5Cn%20Kabupaten%20Ende%5Cn%20Kabupaten%20Flores%20Timur%5Cn%20Kabupaten%20Kupang%5Cn%20Kabupaten%20Lembata%5Cn%20Kabupaten%20Malaka%5Cn%20Kabupaten%20Manggarai%5Cn%20Kabupaten%20Manggarai%20Barat%5Cn%20Kabupaten%20Manggarai%20Timur%5Cn%20Kabupaten%20Nagekeo%5Cn%20Kabupaten%20Ngada%5Cn%20Kabupaten%20Rote%20Ndao%5Cn%20Kabupaten%20Sabu%20Raijua%5Cn%20Kabupaten%20Sikka%5Cn%20Kabupaten%20Sumba%20Barat%5Cn%20Kabupaten%20Sumba%20Barat%20Daya%5Cn%20Kabupaten%20Sumba%20Tengah%5Cn%20Kabupaten%20Sumba%20Timur%5Cn%20Kabupaten%20Timor%20Tengah%20Selatan%5Cn%20Kabupaten%20Timor%20Tengah%20Utara%5Cn%20Kota%20Kupang%5CnKalimantan%20Utara%5Cn%20Kabupaten%20Bulungan%5Cn%20Kabupaten%20Malinau%5Cn%20Kabupaten%20Nunukan%5Cn%20Kabupaten%20Tana%20Tidung%5Cn%20Kota%20Tarakan%5CnKalimantan%20Barat%5Cn%20Kabupaten%20Bengkayang%5Cn%20Kabupaten%20Kapuas%20Hulu%5Cn%20Kabupaten%20Kayong%20Utara%5Cn%20Kabupaten%20Ketapang%5Cn%20Kabupaten%20Kubu%20Raya%5Cn%20Kabupaten%20Landak%5Cn%20Kabupaten%20Melawi%5Cn%20Kabupaten%20Mempawah%5Cn%20Kabupaten%20Sambas%5Cn%20Kabupaten%20Sanggau%5Cn%20Kabupaten%20Sekadau%5Cn%20Kabupaten%20Sintang%5Cn%20Kota%20Pontianak%5Cn%20Kota%20Singkawang%5CnKalimantan%20Tengah%5Cn%20Kabupaten%20Barito%20Selatan%5Cn%20Kabupaten%20Barito%20Timur%5Cn%20Kabupaten%20Barito%20Utara%5Cn%20Kabupaten%20Gunung%20Mas%5Cn%20Kabupaten%20Kapuas%5Cn%20Kabupaten%20Katingan%5Cn%20Kabupaten%20Kotawaringin%20Barat%5Cn%20Kabupaten%20Kotawaringin%20Timur%5Cn%20Kabupaten%20Lamandau%5Cn%20Kabupaten%20Murung%20Raya%5Cn%20Kabupaten%20Pulang%20Pisau%5Cn%20Kabupaten%20Sukamara%5Cn%20Kabupaten%20Seruyan%5Cn%20Kota%20Palangka%20Raya%5CnKalimantan%20Selatan%5Cn%20Kabupaten%20Balangan%5Cn%20Kabupaten%20Banjar%5Cn%20Kabupaten%20Barito%20Kuala%5Cn%20Kabupaten%20Hulu%20Sungai%20Selatan%5Cn%20Kabupaten%20Hulu%20Sungai%20Tengah%5Cn%20Kabupaten%20Hulu%20Sungai%20Utara%5Cn%20Kabupaten%20Kotabaru%5Cn%20Kabupaten%20Tabalong%5Cn%20Kabupaten%20Tanah%20Bumbu%5Cn%20Kabupaten%20Tanah%20Laut%5Cn%20Kabupaten%20Tapin%5Cn%20Kota%20Banjarbaru%5Cn%20Kota%20Banjarmasin%5CnKalimantan%20Timur%5Cn%20Kabupaten%20Berau%5Cn%20Kabupaten%20Kutai%20Barat%5Cn%20Kabupaten%20Kutai%20Kartanegara%5Cn%20Kabupaten%20Kutai%20Timur%5Cn%20Kabupaten%20Mahakam%20Ulu%5Cn%20Kabupaten%20Paser%5Cn%20Kabupaten%20Penajam%20Paser%20Utara%5Cn%20Kota%20Balikpapan%5Cn%20Kota%20Bontang%5Cn%20Kota%20Samarinda%5CnGorontalo%5Cn%20Kabupaten%20Boalemo%5Cn%20Kabupaten%20Bone%20Bolango%5Cn%20Kabupaten%20Gorontalo%5Cn%20Kabupaten%20Gorontalo%20Utara%5Cn%20Kabupaten%20Pohuwato%5Cn%20Kota%20Gorontalo%5CnSulawesi%20Utara%5Cn%20Kabupaten%20Bolaang%20Mongondow%5Cn%20Kabupaten%20Bolaang%20Mongondow%20Selatan%5Cn%20Kabupaten%20Bolaang%20Mongondow%20Timur%5Cn%20Kabupaten%20Bolaang%20Mongondow%20Utara%5Cn%20Kabupaten%20Kepulauan%20Sangihe%5Cn%20Kabupaten%20Kepulauan%20Siau%20Tagulandang%20Biaro%5Cn%20Kabupaten%20Kepulauan%20Talaud%5Cn%20Kabupaten%20Minahasa%5Cn%20Kabupaten%20Minahasa%20Selatan%5Cn%20Kabupaten%20Minahasa%20Tenggara%5Cn%20Kabupaten%20Minahasa%20Utara%5Cn%20Kota%20Bitung%5Cn%20Kota%20Kotamobagu%5Cn%20Kota%20Manado%5Cn%20Kota%20Tomohon%5CnSulawesi%20Barat%5Cn%20Kabupaten%20Majene%5Cn%20Kabupaten%20Mamasa%5Cn%20Kabupaten%20Mamuju%5Cn%20Kabupaten%20Mamuju%20Tengah%5Cn%20Kabupaten%20Pasangkayu%5Cn%20Kabupaten%20Polewali%20Mandar%5CnSulawesi%20Tengah%5Cn%20Kabupaten%20Banggai%5Cn%20Kabupaten%20Banggai%20Kepulauan%5Cn%20Kabupaten%20Banggai%20Laut%5Cn%20Kabupaten%20Buol%5Cn%20Kabupaten%20Donggala%5Cn%20Kabupaten%20Morowali%5Cn%20Kabupaten%20Morowali%20Utara%5Cn%20Kabupaten%20Parigi%20Moutong%5Cn%20Kabupaten%20Poso%5Cn%20Kabupaten%20Sigi%5Cn%20Kabupaten%20Tojo%20Una-Una%5Cn%20Kabupaten%20Tolitoli%5Cn%20Kota%20Palu%5CnSulawesi%20Selatan%5Cn%20Kabupaten%20Bantaeng%5Cn%20Kabupaten%20Barru%5Cn%20Kabupaten%20Bone%5Cn%20Kabupaten%20Bulukumba%5Cn%20Kabupaten%20Enrekang%5Cn%20Kabupaten%20Gowa%5Cn%20Kabupaten%20Jeneponto%5Cn%20Kabupaten%20Kepulauan%20Selayar%5Cn%20Kabupaten%20Luwu%5Cn%20Kabupaten%20Luwu%20Timur%5Cn%20Kabupaten%20Luwu%20Utara%5Cn%20Kabupaten%20Maros%5Cn%20Kabupaten%20Pangkajene%20dan%20Kepulauan%5Cn%20Kabupaten%20Pinrang%5Cn%20Kabupaten%20Sidenreng%20Rappang%5Cn%20Kabupaten%20Sinjai%5Cn%20Kabupaten%20Soppeng%5Cn%20Kabupaten%20Takalar%5Cn%20Kabupaten%20Tana%20Toraja%5Cn%20Kabupaten%20Toraja%20Utara%5Cn%20Kabupaten%20Wajo%5Cn%20Kota%20Makassar%5Cn%20Kota%20Palopo%5Cn%20Kota%20Parepare%5CnSulawesi%20Tenggara%5Cn%20Kabupaten%20Bombana%5Cn%20Kabupaten%20Buton%5Cn%20Kabupaten%20Buton%20Selatan%5Cn%20Kabupaten%20Buton%20Tengah%5Cn%20Kabupaten%20Buton%20Utara%5Cn%20Kabupaten%20Kolaka%5Cn%20Kabupaten%20Kolaka%20Timur%5Cn%20Kabupaten%20Kolaka%20Utara%5Cn%20Kabupaten%20Konawe%5Cn%20Kabupaten%20Konawe%20Kepulauan%5Cn%20Kabupaten%20Konawe%20Selatan%5Cn%20Kabupaten%20Konawe%20Utara%5Cn%20Kabupaten%20Muna%5Cn%20Kabupaten%20Muna%20Barat%5Cn%20Kabupaten%20Wakatobi%5Cn%20Kota%20Bau-Bau%5Cn%20Kota%20Kendari%5CnMaluku%20Utara%5Cn%20Kabupaten%20Halmahera%20Barat%5Cn%20Kabupaten%20Halmahera%20Tengah%5Cn%20Kabupaten%20Halmahera%20Timur%5Cn%20Kabupaten%20Halmahera%20Selatan%5Cn%20Kabupaten%20Halmahera%20Utara%5Cn%20Kabupaten%20Kepulauan%20Sula%5Cn%20Kabupaten%20Pulau%20Morotai%5Cn%20Kabupaten%20Pulau%20Taliabu%5Cn%20Kota%20Ternate%5Cn%20Kota%20Tidore%20Kepulauan%5CnMaluku%5Cn%20Kabupaten%20Buru%5Cn%20Kabupaten%20Buru%20Selatan%5Cn%20Kabupaten%20Kepulauan%20Aru%5Cn%20Kabupaten%20Maluku%20Barat%20Daya%5Cn%20Kabupaten%20Maluku%20Tengah%5Cn%20Kabupaten%20Maluku%20Tenggara%5Cn%20Kabupaten%20Kepulauan%20Tanimbar%5Cn%20Kabupaten%20Seram%20Bagian%20Barat%5Cn%20Kabupaten%20Seram%20Bagian%20Timur%5Cn%20Kota%20Ambon%5Cn%20Kota%20Tual%5CnPapua%20Barat%5Cn%20Kabupaten%20Fakfak%5Cn%20Kabupaten%20Kaimana%5Cn%20Kabupaten%20Manokwari%5Cn%20Kabupaten%20Manokwari%20Selatan%5Cn%20Kabupaten%20Maybrat%5Cn%20Kabupaten%20Pegunungan%20Arfak%5Cn%20Kabupaten%20Raja%20Ampat%5Cn%20Kabupaten%20Sorong%5Cn%20Kabupaten%20Sorong%20Selatan%5Cn%20Kabupaten%20Tambrauw%5Cn%20Kabupaten%20Teluk%20Bintuni%5Cn%20Kabupaten%20Teluk%20Wondama%5Cn%20Kota%20Sorong%5CnPapua%5Cn%20Kabupaten%20Asmat%5Cn%20Kabupaten%20Biak%20Numfor%5Cn%20Kabupaten%20Boven%20Digoel%5Cn%20Kabupaten%20Deiyai%5Cn%20Kabupaten%20Dogiyai%5Cn%20Kabupaten%20Intan%20Jaya%5Cn%20Kabupaten%20Jayapura%5Cn%20Kabupaten%20Jayawijaya%5Cn%20Kabupaten%20Keerom%5Cn%20Kabupaten%20Kepulauan%20Yapen%5Cn%20Kabupaten%20Lanny%20Jaya%5Cn%20Kabupaten%20Mamberamo%20Raya%5Cn%20Kabupaten%20Mamberamo%20Tengah%5Cn%20Kabupaten%20Mappi%5Cn%20Kabupaten%20Merauke%5Cn%20Kabupaten%20Mimika%5Cn%20Kabupaten%20Nabire%5Cn%20Kabupaten%20Nduga%5Cn%20Kabupaten%20Paniai%5Cn%20Kabupaten%20Pegunungan%20Bintang%5Cn%20Kabupaten%20Puncak%5Cn%20Kabupaten%20Puncak%20Jaya%5Cn%20Kabupaten%20Sarmi%5Cn%20Kabupaten%20Supiori%5Cn%20Kabupaten%20Tolikara%5Cn%20Kabupaten%20Waropen%5Cn%20Kabupaten%20Yahukimo%5Cn%20Kabupaten%20Yalimo%5Cn%20Kota%20Jayapura%22%7D%5D" data-version="2">
                            </div>
                            <script type="text/javascript">
                            setTimeout(function()
                            {
                                var _cFieldFrame = document.getElementById("customFieldFrame_17");
                                if (_cFieldFrame)
                                {
                                    _cFieldFrame.onload = function()
                                    {
                                        if (typeof widgetFrameLoaded !== 'undefined')
                                        {
                                            widgetFrameLoaded(17, {
                                                "formID": 202011737401439
                                            })
                                        }
                                    };
                                    _cFieldFrame.src = "//widgets.jotform.io/dynamicDropdowns/?qid=17&ref=" + encodeURIComponent(window.location.protocol + "//" + window.location.host) + '' + '&injectCSS=' + encodeURIComponent(window.location.search.indexOf("ndt=1") > -1);
                                    _cFieldFrame.addClassName("custom-field-frame-rendered");
                                }
                            }, 0);
                            </script>
                        </div>
                    </div>
                </li>
                <li class="form-line jf-required" data-type="control_dropdown" id="id_12">
                    <label class="form-label form-label-top form-label-auto" id="label_12" for="input_12">
                        Model
                        <span class="form-required">
                            *
                        </span>
                    </label>
                    <div id="cid_12" class="form-input-wide jf-required">
                        <select class="form-dropdown validate[required]" id="input_12" name="q12_model" style="width:150px" data-component="dropdown" required="" aria-labelledby="label_12">
                            <option value=""> Pilih Model </option>
                            <option value="City"> City </option>
                            <option value="Civic Type R"> Civic Type R </option>
                            <option value="Odyssey"> Odyssey </option>
                            <option value="Civic Hatchback"> Civic Hatchback </option>
                            <option value="Jazz"> Jazz </option>
                            <option value="Brio"> Brio </option>
                            <option value="CR-V"> CR-V </option>
                            <option value="Civic"> Civic </option>
                            <option value="Mobilio"> Mobilio </option>
                            <option value="BR-V"> BR-V </option>
                            <option value="Accord"> Accord </option>
                            <option value="HR-V"> HR-V </option>
                        </select>
                    </div>
                </li>
                <li class="form-line" data-type="control_text" id="id_14">
                    <div id="cid_14" class="form-input-wide">
                        <div id="text_14" class="form-html" data-component="text">
                            <p><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 8pt;">Dengan mengirimkan, Saya Setuju dengan&nbsp;<a href="https://www.mobil123.com/pdpn" target="_blank" rel="nofollow">Perlindungan Data Pribadi</a> dan <a href="https://www.mobil123.com/kebijakan-privasi" target="_blank" rel="nofollow">Kebijakan Privasi</a> PT Mobil Satu Asia termasuk pengumpulan, penggunaan, pengungkapan, pemrosesan, penyimpanan dan penanganan informasi pribadi saya; dan komunikasi pemasaran langsung dari Anda dan / atau mitra Anda.</span></p>
                        </div>
                    </div>
                </li>
                <li class="form-line" data-type="control_button" id="id_2">
                    <div id="cid_2" class="form-input-wide">
                        <div style="text-align:center" data-align="center" class="form-buttons-wrapper form-buttons-center   jsTest-button-wrapperField">
                            <button id="input_2" type="submit" class="form-submit-button form-submit-button-3d_edgy_yellow submit-button jf-form-buttons jsTest-submitField" data-component="button" data-content="">
                                pesan
                            </button>
                        </div>
                    </div>
                </li>
                <li style="display:none">
                    Should be Empty:
                    <input type="text" name="website" value="">
                </li>
            </ul>
        </div>
        <script>
        JotForm.showJotFormPowered = "0";
        </script>
        <script>
        JotForm.poweredByText = "Powered by JotForm";
        </script>
        <input type="hidden" id="simple_spc" name="simple_spc" value="202011737401439-202011737401439">
        <script type="text/javascript">
        document.getElementById("si" + "mple" + "_spc").value = "202011737401439-202011737401439";
        </script>
        <input type="hidden" name="embedUrl" value="https://www.mobil123.com/form/form-booking-honda-dve2020"><input type="hidden" name="event_id" value="1597635452160_202011737401439_K0lTTnE"></form>
        <?php include 'views/templates/modal-end.php'; ?>
        <!--MODAL:BOOKING-->
