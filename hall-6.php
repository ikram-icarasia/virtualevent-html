<?php
    include 'views/core/head.php';
    $hall_size = 6;
?>

<section class="platform  platform-hall  hall-<?=$hall_size?>  embed-responsive embed-responsive-16by9">
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="assets/halls/hall-<?=$hall_size?>.mp4" type="video/mp4" />
    </video>

    <?php include 'views/templates/cinema-billboards.php'; ?>

    <?php for($booth = 1; $booth <= $hall_size; $booth++ ) { ?>
    <a href="#" class="booth  booth-<?=$booth?>">
        <div class="booth-logo"><img src="assets/cars/honda/honda-logo.png" class="d-block" /></div>
        <div class="booth-tv"><img src="https://media.giphy.com/media/xT9DPDoWMicL4nU3NC/source.gif" class="d-block" /></div>
        <?php for($booth_car = 1; $booth_car <= 5; $booth_car++ ) { ?>
            <div class="booth-car booth-car-<?=$booth_car?>  car">
                <img class="car-image" src="assets/cars/honda/cars/honda-car-<?= rand(1,4); ?>.png" class="block" />
            </div>
        <?php } ?>
    </a>
    <?php } ?>
</section>

<?php include 'views/templates/toolbar.php'; ?>
<?php include 'views/core/foot.php'; ?>
